
## fdilink的imu驱动包
Deta-10-ros-v0.0.1
### 依赖：
```bash
sudo apt install ros-melodic-serial
```

### 使用：    
ahrs_driver.launch
```
<launch>
  <node pkg="fdilink_ahrs" name="ahrs_driver" type="ahrs_driver" output="screen" >
    <!-- 是否输出debug信息 -->
    <param name="debug"  value="false"/>
    
    <!-- 串口设备，可通过rules.d配置固定 -->
    <param name="port"  value="/dev/ttyUSB0"/>
    <!-- <param name="port"  value="/dev/ttyTHS1"/> -->

    <!-- 波特率 -->
    <param name="baud"  value="921600"/>

    <!-- 发布的imu话题名 -->
    <param name="imu_topic"  value="/imu"/>
    
    <!-- 发布的imu话题中的frame_id -->
    <param name="imu_frame"  value="imu"/>

    <!-- 地磁北的yaw角 --> # 二维指北的朝向，北为0，逆时针增加，0~2π的取值范围。
    <param name="mag_pose_2d_topic"  value="/mag_pose_2d"/>

    <!-- 发布的数据基于不同设备有不同的坐标系   -->
    <param name="device_type"  value="1"/> <!-- 0: origin_data, 1: for single imu or ucar in ROS, 2:for Xiao in ROS -->
  </node>
</launch> 
```
  其中`device_type`：

  0. Deta-10的原始坐标系模式
  1. 单独imu的坐标系模式

调用的ahrs_driver节点会发布`sensor_msgs/Imu`格式的imu topic。
```
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
geometry_msgs/Quaternion orientation
  float64 x
  float64 y
  float64 z
  float64 w
float64[9] orientation_covariance
geometry_msgs/Vector3 angular_velocity
  float64 x
  float64 y
  float64 z
float64[9] angular_velocity_covariance
geometry_msgs/Vector3 linear_acceleration
  float64 x
  float64 y
  float64 z
float64[9] linear_acceleration_covariance
```
也会发布`geometry_msgs/Pose2D`格式的二维指北角话题，话题名默认为`/mag_pose_2d`。
```
float64 x
float64 y
float64 theta  # 指北角
```

### 2020-1-15
  维护了文件注释。

### 2020-10-20
  添加了`device_type`参数，可以在`ahrs_driver.launch`文件中指定设备类型，根据不同设备类型以不同的坐标系发布ROS的imu数据。
  其中：

    0. Deta-10的原始坐标系模式
    1. 单独imu的坐标系模式



#### 介绍

使用飞迪惯导传感器获取航向和室外的绝对定位。（传感器型号：DETA100D4G）使用两张4G流量卡，默认5年套餐

#### 软件架构

使用ROS系统

#### 安装教程

1. sudo apt install ros-melodic-serial
2. cd ~/catkin_ws/  
3. catkin_make
4. udev端口绑定（KERNEL=="ttyUSB*", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", MODE:="0777", GROUP:="dialout", SYMLINK+="fdilink_ahrs"）
5. roslaunch fdilink_ahrs ahrs_driver.launch

#### 使用说明

1. 将三脚架基站架设到室外空旷位置，打开电源，显示进入3D mode.
2. 惯导安装天线放在塔台两边支架上，模块三根天线头朝车头，串口头朝车尾，底座安装于车身，水平安装。使用上位机设置msg_gnss_dual_ant(0x78)反馈频率5HZ，上位机设置进入RTK fixed模式的阈值为0.05m。使用保存参数，重启设备。上位机查看右边的GPS Qual:RTK fixed,HAcc:0.01 VAcc:0.01.说明当前位置可以进入差分模式。
3. 右边的天线为主天线，顺时针旋转为车身航向(0~360)。
4. 车身初始停靠位置为车头朝正北，惯导上电,这时候车身角度为零度(0~360)。
5. 我们使用终端运行命令：roslaunch fdilink_ahrs ahrs_driver.launch，终端不在显示**NO RTK FIXED !!!!** ，说明进入RTK fixed，这时候节点将发出/odom位置信息。(在驱动终端会显示MBhAcc和MBvAcc值小于0.02，Heading值正北方向时，Heading>6.0|Heading<0.34)。

